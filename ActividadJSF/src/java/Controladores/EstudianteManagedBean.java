package Controladores;

import Entidades.Estudiante;
import Modelos.EstudianteModel;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;


@Named(value = "BEstudiante")
@SessionScoped
public class EstudianteManagedBean implements Serializable {

    private Estudiante estu = new Estudiante();
    private EstudianteModel esmodel = new EstudianteModel();
    
    public EstudianteManagedBean() {
    
    }

    public Estudiante getEstu() {
        return estu;
    }

    public void setEstu(Estudiante estu) {
        this.estu = estu;
    }

  
   
    public String Agregar() {
        esmodel.registrar(this.estu);
        this.estu = new Estudiante();
        
        return "index?faces-redirect=true";
    }

    public String Editar() {
        esmodel.editar(this.estu);
        this.estu = new Estudiante();
        
        return "index?faces-redirect=true";
    }

    public String Eliminar() {
        esmodel.eliminar(this.estu);
        this.estu = new Estudiante();
        
        return "index?faces-redirect=true";
    }
    
    public List<Estudiante> getListaEstudiante() {
        return esmodel.listaa();
    }
}
