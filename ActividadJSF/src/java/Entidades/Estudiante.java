package Entidades;


public class Estudiante {

private int id;
private String Nombre;
private int Edad;

    public Estudiante() {
    }
   
    
public Estudiante(int id, String Nombre, int Edad) {
        this.id = id;
        this.Nombre = Nombre;
        this.Edad = Edad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int Edad) {
        this.Edad = Edad;
    }



}
