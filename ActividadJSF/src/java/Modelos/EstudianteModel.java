package Modelos;

import Entidades.Estudiante;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class EstudianteModel {

  List<Estudiante> lista = new ArrayList<>();  

public List<Estudiante> listaa() {
        if (!lista.isEmpty()) {
            System.out.println(lista.size());
        } else {
            lista.add(new Estudiante(1, "Ismael Menjivar", 18));
            lista.add(new Estudiante(2, "Alvaro Valladares", 20));
            lista.add(new Estudiante(3, "Jeferson", 22));
        }
        return lista;
    }

public void registrar(Estudiante e) {
        if(e.getId() > 0 && e.getNombre().length() > 0){
            lista.add(e);
            System.out.println("Agregado");
        }
    }

    public void editar(Estudiante e) {
        Iterator<Estudiante> i = lista.iterator();
        int index = 0;
        while(i.hasNext()){
            Estudiante est = i.next();
            if(est.getId() == e.getId()){
                if(e.getId() > 0 && e.getNombre().length() > 0){
                    lista.set(index, e);
                    System.out.println("Editado");
                }
            }
            index++;
        }
    }

    public void eliminar(Estudiante e) {
        Iterator<Estudiante> i = lista.iterator();
        while(i.hasNext()){
            Estudiante est = i.next();
            
            if(est.getId() == e.getId()){
                System.out.println("ID ELIMINADO: " + est.getId());
                i.remove();
            }
        }
    }
    
}
